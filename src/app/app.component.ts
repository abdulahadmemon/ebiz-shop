import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Device } from '@ionic-native/device';
import { PayPal } from '@ionic-native/paypal';

import { UserProvider } from '../providers/user/user';

import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { MessageListPage } from '../pages/message-list/message-list';
import { ProfilePage } from '../pages/profile/profile';
import { Storage } from '@ionic/storage';
import { ImageLoaderConfig } from 'ionic-image-loader';
// import { CacheService } from 'ionic-cache';
@Component({
  templateUrl: 'app.html',
  providers: [Device, Push]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, icon: string, component: any }>;
  animate: any = { animate: true, direction: 'forward' };

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private push: Push,
    private device: Device,
    private userProvider: UserProvider,
    private payPal: PayPal,
    private imageLoaderConfig: ImageLoaderConfig,
    private storage: Storage

  ) {

    if (this.userProvider.getUser() == null)
      this.rootPage = LoginPage;
    else
      this.rootPage = HomePage;

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', icon: 'assets/icon/home.png', component: HomePage },
      { title: 'Messages', icon: 'assets/icon/chat.png', component: MessageListPage },
      { title: 'My Profile', icon: 'assets/icon/resume.png', component: ProfilePage },
      { title: 'About', icon: 'assets/icon/conversation.png', component: HomePage },
      { title: 'Help', icon: 'assets/icon/support.png', component: HomePage },
      { title: 'Terms & Conditions', icon: 'assets/icon/list.png', component: HomePage },
      { title: 'Logout', icon: 'assets/icon/logout.png', component: '' }
    ];

    platform.ready().then(() => {
storage.get('home_deals').then((val)=>{
  console.log(val)
})
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.payPal.init({
      //   PayPalEnvironmentProduction: 'ASFgGBimPx1TRklADfkhc0zdAxojZsl9vs5T7cmQaD_BRRQFUm6CCv9_LLpaYY4ChGtXFP1Fm7czszao',
      //   PayPalEnvironmentSandbox: 'YOUR_SANDBOX_CLIENT_ID'
      // }).then(() => {
      //   // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      //   this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
      //     // Only needed if you get an "Internal Service Error" after PayPal login!
      //     //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      //   })).then((prep) => {
      //     console.log('~~ Prepare: ~~~')
      //     console.log(prep);
      //     let payment = new PayPalPayment('3.33', 'USD', 'Description', 'sale');
      //     this.payPal.renderSinglePaymentUI(payment).then((res) => {
      //       console.log('~~ RESPONSE: ~~~')
      //       console.log(res);
      //       // Successfully paid

      //       // Example sandbox response
      //       //
      //       // {
      //       //   "client": {
      //       //     "environment": "sandbox",
      //       //     "product_name": "PayPal iOS SDK",
      //       //     "paypal_sdk_version": "2.16.0",
      //       //     "platform": "iOS"
      //       //   },
      //       //   "response_type": "payment",
      //       //   "response": {
      //       //     "id": "PAY-1AB23456CD789012EF34GHIJ",
      //       //     "state": "approved",
      //       //     "create_time": "2016-10-03T13:33:33Z",
      //       //     "intent": "sale"
      //       //   }
      //       // }
      //     }, () => {
      //       // Error or render dialog closed without being successful
      //     });
      //   }, () => {
      //     // Error in configuration
      //   });
      // }, () => {
      //   // Error in initialization, maybe PayPal isn't supported or something else
      // });
      if (this.device.platform == null)
        return;
      this.imageLoaderConfig.enableDebugMode();
      this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
      this.imageLoaderConfig.setFallbackUrl('./assets/imgs/loader.gif');
      this.imageLoaderConfig.setMaximumCacheAge(24 * 60 * 60 * 1000);
      this.imageLoaderConfig.setImageReturnType('base64');

      // cache.setDefaultTTL(60 * 60 * 12);
      // cache.setOfflineInvalidate(false);
      statusBar.backgroundColorByHexString('#07a5ff');
      splashScreen.hide();
      this.push.hasPermission().then((res: any) => {
        if (res.isEnabled) {
          console.log('hasPermission')
          console.log('res: ' + res)
        }
        else {
          console.log('We do not have permission to send push notifications');
        }
      });
      const options: PushOptions = {
        android: {
          senderID: '116674096622',
          forceShow: true,
          sound: true,
          vibrate: true,
        },
        ios: {
          alert: true,
          badge: true,
          sound: true
        },
        windows: {}
      };

      const pushObject: PushObject = this.push.init(options);
      pushObject.on('registration').subscribe(
        (registration: any) => {
          console.log('registration');
          let device_info = { gcm_id: registration.registrationId, platform: this.device.platform };
          localStorage.setItem('device_info', JSON.stringify(device_info));
          if (this.userProvider.getUser() != null) {
            let tmp = { userId: this.userProvider.getUser()._id, gcm_id: device_info.gcm_id, platform: device_info.platform };
            this.userProvider.updateGCM(tmp).subscribe(
              response => console.log(response),
              error => console.log(error)
            );
          }
        }
      );

      pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
      pushObject.on('notification').subscribe(
        (notification: any) => {
          console.log('Received a notification', notification);
          console.log('type: ' + notification.additionalData.type);
          if (notification.additionalData.type == "new message")
            this.nav.push(MessageListPage, { type: 'pushData', notiData: notification.additionalData });
          // if(notification.additionalData.type == 'new message')
          // console.log("yaha",{type:'pushData', notiData:notification.additionalData.data});
          // this.nav.push(MessageListPage);
          // this.nav.push(MessageListPage);
        }
      );
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.title == 'Logout') {
      this.storage.remove('home_deals').then(()=>{
        console.log("successfully logout")
      })
       this.logout();
    }
    else {
      this.nav.setRoot(page.component, {}, this.animate);
    }
  }

  public logout(): void {
    this.userProvider.removeUser().then((res) => {
      if (res)
        this.nav.setRoot(LoginPage, {}, this.animate);
    });
  }
}

