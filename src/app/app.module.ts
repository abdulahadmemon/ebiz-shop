import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Push } from '@ionic-native/push';
import { Camera } from '@ionic-native/camera';
import { Keyboard } from '@ionic-native/keyboard';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PayPal } from '@ionic-native/paypal';
import { File } from '@ionic-native/file';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { UploadDealsPage } from '../pages/upload-deals/upload-deals';
import { DealDetailsPage } from '../pages/deal-details/deal-details';
import { MessageListPage } from '../pages/message-list/message-list';
import { SendMessagePage } from '../pages/send-message/send-message';
import { ProfilePage } from '../pages/profile/profile';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';

import { UserProvider } from '../providers/user/user';
import { DealsProvider } from '../providers/deals/deals';
import { MessageProvider } from '../providers/message/message';
import { GlobalVariablesProvider } from '../providers/global-variables/global-variables';

import { LazyLoadImageModule } from 'ng-lazyload-image';
import { TimeAgoPipe } from 'time-ago-pipe';
import { IonicImageLoader } from 'ionic-image-loader';

import { IonicStorageModule } from '@ionic/storage';
import { CacheModule } from 'ionic-cache';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    UploadDealsPage,
    DealDetailsPage,
    MessageListPage,
    SendMessagePage,
    ProfilePage,
    EditProfilePage,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    HttpModule,
    LazyLoadImageModule,
    HttpClientModule,
    IonicImageLoader.forRoot(),
    CacheModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    UploadDealsPage,
    DealDetailsPage,
    MessageListPage,
    SendMessagePage,
    ProfilePage,
    EditProfilePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Push,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    DealsProvider,
    MessageProvider,
    Keyboard,
    SocialSharing,
    GlobalVariablesProvider,
    PayPal,
    File,

  ]
})
export class AppModule {}
