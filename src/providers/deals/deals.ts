import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { GlobalVariablesProvider } from '../global-variables/global-variables';
import { UserProvider } from '../user/user';

@Injectable()
export class DealsProvider {

  private baseUrl:string = this.globalVariables.getEndpoint()+"api/deals/";
  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(public http: Http,
              public globalVariables: GlobalVariablesProvider,
              public userProvider: UserProvider) {

    console.log('Hello DealsProvider Provider');
  }

  public postNewDeal(data: any): Observable<any> {
    let bodyString = this.serialize(data);
    this.headers.set('x-auth-token', this.userProvider.getUser().token);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'upload', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  public getAllDeals(data: any): Observable<any> {
    let bodyString = this.serialize(data);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'getAll', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  public deleteDeal(data: any): Observable<any> {
    let bodyString = this.serialize(data);
    this.headers.set('x-auth-token', this.userProvider.getUser().token);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'delete', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  public editDeal(data: any): Observable<any> {
    let bodyString = this.serialize(data);
    this.headers.set('x-auth-token', this.userProvider.getUser().token);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'edit', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  private serialize(data):any {
    var buffer = [];
    // Serialize each key in the object.
    for (var name in data) {
      if(!data.hasOwnProperty(name))
        continue;
      var value = data[ name ];
      buffer.push(encodeURIComponent( name ) + "=" + encodeURIComponent( ( value == null ) ? "" : value ));
    }
    // Serialize the buffer and clean it up for transportation.
    var source = buffer.join( "&" ).replace( /%20/g, "+" );
    return(source);
  }
}
