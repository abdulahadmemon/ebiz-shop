import { Injectable } from '@angular/core';

@Injectable()
export class GlobalVariablesProvider {

  public env:string = 'Dev';
  // office:http://192.168.18.159:4201/
  // home:http://192.168.169.101:4201/
  public getEndpoint():string {
    if(this.env == 'Dev')
      // return 'http://192.168.18.159:4200/';
         return 'http://192.168.0.106:4200/';
    else
      return 'http://skapi.smartlybiz.com/';
  }
  // constructor() {
  //   console.log('Hello GlobalVariablesProvider Provider');
  // }
}
