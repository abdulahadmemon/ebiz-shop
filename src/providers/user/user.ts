import { Injectable } from '@angular/core';
// import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { GlobalVariablesProvider } from '../global-variables/global-variables';

@Injectable()
export class UserProvider {

  private baseUrl:string = this.globalVariables.getEndpoint()+"api/user/";
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(public http: Http,
              private _http: HttpClient,
              public globalVariables: GlobalVariablesProvider) {
    console.log('Hello UserProvider Provider');
  }

  public signin(data: any): Observable<any> {
    let bodyString = JSON.stringify(data);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'login', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json())); //...errors if an
  }

  public signup(data: any): Observable<any> {
    let bodyString = JSON.stringify(data);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'signup', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  public edit(data: any): Observable<any> {
    delete data.token;
    let bodyString = JSON.stringify(data);
    this.headers.set('x-auth-token', this.getUser().token);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'edit', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  public updateGCM(data: any): Observable<any> {
    let bodyString = JSON.stringify(data);
    this.headers.set('x-auth-token', this.getUser().token);
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'updategcm', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  public getUpdatedUser(): Observable<any> {
    let bodyString = JSON.stringify({_id: this.getUser()._id});
    let options = new RequestOptions({ headers: this.headers }); // Create a request option
    return this.http.post(this.baseUrl + 'getUser', bodyString, options) // ...using post request
    .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if an
  }

  public saveUser(data:any):void {
    localStorage.setItem('smartlyBiz_shop', JSON.stringify(data));
  }

  public getUser():UserObj {
    let data = JSON.parse(localStorage.getItem('smartlyBiz_shop'));
    return data;
  }

  public removeUser(): Promise<boolean> {
    return new Promise ((res)=> {
      localStorage.removeItem('smartlyBiz_shop');
      return res(true);
    })
  }

  public getToken(data:any):void {
    const token = JSON.parse(localStorage.getItem('smartlyBiz_shop')).token;
    return token;
  }

  private serialize(data):any {
    var buffer = [];
    // Serialize each key in the object.
    for (var name in data) {
      if(!data.hasOwnProperty(name))
        continue;
      var value = data[ name ];
      buffer.push(encodeURIComponent( name ) + "=" + encodeURIComponent( ( value == null ) ? "" : value ));
    }
    // Serialize the buffer and clean it up for transportation.
    var source = buffer.join( "&" ).replace( /%20/g, "+" );
    return(source);
   }
}

export interface ReturnResponse {
  success?: boolean,
  data?: Object,
  msg?: string
}

export interface UserObj {
  _id?:string,
  fullname?:string,
  userName?:string,
  email?:string,
  mobile?:number,
  password?:string,
  platform?:string,
  gcm_id?:string,
  companyName?:string,
  websiteUrl?:string,
  facebookUrl?:string,
  qrcodePath?:string,
  is_premium?:string,
  createdDate?:string,
  token?:string,
  credits?:number
}
