import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Content } from 'ionic-angular';

import { MessageProvider } from '../../providers/message/message';
import { GlobalVariablesProvider } from '../../providers/global-variables/global-variables';

import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { Keyboard } from '@ionic-native/keyboard';

@IonicPage()
@Component({
  selector: 'page-send-message',
  templateUrl: 'send-message.html',
})
export class SendMessagePage {

  @ViewChild(Content) content: Content;

  private dealObjectParam:any = this.navParams.get('data');
  private image = this.navParams.get('image');
  private is_loaded:boolean = true;
  public  data:any;
  private messageText:string;
  private is_empty:boolean = false;
  private show_spinner:boolean = false;
  private timeInterval:any;
  private previousMessagesArrayLength:number = 0;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private messageProvider: MessageProvider,
              private keyboard: Keyboard,
              public _DomSanitizationService: DomSanitizer,
              public globalVariables: GlobalVariablesProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SendMessagePage',this.navParams);
    this.keyboard.onKeyboardShow().subscribe(
      response=> this.content.scrollToBottom()
    );

    let loader = this.loadingCtrl.create({
      spinner:'dots',
      content:'Loading Chat...',
      duration:10000,
      dismissOnPageChange:true
    });
    loader.present();
    this.messageProvider.check(this.dealObjectParam).subscribe(
      response=> {
        this.is_loaded = true;
        loader.dismiss();
        this.data = response.data;
        this.previousMessagesArrayLength = this.data.message.length;
        setTimeout(()=> {
          this.content.scrollToBottom();
        },300)
        console.log(this.data);
      },
      error=> {
        loader.dismiss();
        console.error(error);
      }
    );
    loader.onDidDismiss(()=> {
      if(this.is_loaded)
        return;
      this.alertCtrl.create({
        title:'Request timed out!!',
        subTitle:'Something went wrong.',
        message:'Tryagain later and check your internet connection.'
      }).present();
    });
    this.checkMessages();
  }

  ionViewWillLeave() {
    clearInterval(this.timeInterval);
  }

  private checkMessages():void {
    this.timeInterval = setInterval(()=>{
      this.messageProvider.check(this.dealObjectParam).subscribe(
        response=> {
          // setTimeout(()=>{
          //   this.checkMessages();
          // }, 3000);
          this.is_loaded = true;
          this.data = response.data;
          console.log(this.data)
          if(this.previousMessagesArrayLength < this.data.message.length) {
            setTimeout(()=> {
              this.previousMessagesArrayLength = this.data.message.length
              this.content.scrollToBottom();
            },300)
          }
        },
        error=> {
          console.error(error);
          // this.checkMessages();
        }
      );
    }, 3000)
  }

  public postMessage():void {
    if(this.messageText == undefined || this.messageText == '') {
      this.is_empty = true;
      setTimeout(()=> {
        this.is_empty = false;
      },1000);
      return;
    }
    this.show_spinner = true;
    let d = new Date();
    this.dealObjectParam.message = {
      type: 'shopkeeper',
      time: d,
      text: this.messageText
    }

    this.messageProvider.send(this.dealObjectParam).subscribe(
      response=> {
        this.show_spinner = false;
        this.data.message.push(this.dealObjectParam.message);
        console.log(response);
        setTimeout(()=> {
          this.content.scrollToBottom();
          this.messageText = '';
        },500);
      },
      error=> {
        this.show_spinner = false;
        console.error(error);
        this.alertCtrl.create({
          title: error.code,
          subTitle:error.msg,
          message:"Try logout and login again.",
          enableBackdropDismiss: false,
          buttons:[
            {
              text:'Okay',
              handler:() => {
                this.navCtrl.pop();
              }
            }
          ]
        }).present();
      }
    )
  }

  convertImage(imagePath):SafeUrl {
    if(imagePath.includes('skapi') || imagePath.includes('192'))
      return this._DomSanitizationService.bypassSecurityTrustUrl(imagePath);
    else
      return this._DomSanitizationService.bypassSecurityTrustUrl(this.globalVariables.getEndpoint()+imagePath);

  }
}
