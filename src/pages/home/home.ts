import { Component } from '@angular/core';
import { NavController, ToastController, MenuController, AlertController, LoadingController } from 'ionic-angular';

import { UploadDealsPage } from '../upload-deals/upload-deals';
import { DealDetailsPage } from '../deal-details/deal-details';
import { MessageListPage } from '../message-list/message-list';
import { ProfilePage } from '../profile/profile';

import { DealsProvider } from '../../providers/deals/deals';
import { UserProvider } from '../../providers/user/user';
import { GlobalVariablesProvider } from '../../providers/global-variables/global-variables';

import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { SocialSharing } from '@ionic-native/social-sharing';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public data:any = [];
  animate:any = {animate: true, direction: 'backward'};

  constructor(private navCtrl: NavController,
              private dealsProvider: DealsProvider,
              private userProvider: UserProvider,
              private toastCtrl: ToastController,
              private _DomSanitizationService: DomSanitizer,
              private menuCtrl: MenuController,
              private socialSharing: SocialSharing,
              public globalVariables: GlobalVariablesProvider,
              private storage: Storage,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController
              ) {}

  ngOnInit() {
    this.storage.get('home_deals').then((val) => {
      if(val) {
        this.data = val;
        console.log(this.data)
        // for(var i = 0; i < val.length; i++) {
        //   console.log(val[i].image);
        //   val[i].image = this.changeURL(val[i].image);
        //   if(i+1 == val.length)
        //     this.data = val;
        // }
      }
    });
    this.loadDeals();
    this.menuCtrl.enable(true, 'myMenu');
  }

  public changeURL(image):SafeUrl {
    if(image.includes('skapi') || image.includes('192')) {
      let tmp = image;
      return (tmp);
    }
    else {
      let tmp = this.globalVariables.getEndpoint()+ image;
      return (tmp);
    }
    // let sanitize = this._DomSanitizationService.bypassSecurityTrustUrl(tmp);
    // console.log(tmp);
  }

  public trimDescriptionText(text:string):string {
    //trim the string to the maximum length
    let trimmedString = text.substr(0, 97);

    //re-trim if we are in the middle of a word
    // trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));

    //check whether to concatinate '...'
    if(text.length > trimmedString.length)
      return trimmedString + ' ...';
    else
      return trimmedString;
  }

  private loadDeals():void {
    console.log('loadDeals')
    let tmp = { storeId: this.userProvider.getUser()._id }
    this.dealsProvider.getAllDeals(tmp).subscribe(
      response=> {
        if(response.success){
          for(var i = 0; i < response.data.length; i++) {
            console.log(response.data[i].image);
            response.data[i].image = this.changeURL(response.data[i].image);
            if(i+1 == response.data.length)
              this.data = response.data;
          }

          this.storage.set('home_deals', response.data).then((res)=> {
            console.log('res: '+ res);
            // this.storage.get('home_deals').then((val) => {
            //   console.log('Your name is', val);
            // });
          });;
        }
        else {
          if(response.msg == 'No Deals Found') {
            this.storage.remove('home_deals');
            return;
          }
          const toast = this.toastCtrl.create({
            position:'top',
            showCloseButton:true,
            closeButtonText: 'Reload',
            dismissOnPageChange:true,
            cssClass:'custom-toast',
            message:'Error loading data!!'
          });

          toast.onDidDismiss(()=> {
            this.loadDeals();
          });

          toast.present();
        }
      }
    );
  }
  public onImageLoad($event) {
    // console.log('onImageLoad:');
    // console.log($event);
  }

  public uplaodDeal(fab):void {
    fab.close();
    this.navCtrl.setRoot(UploadDealsPage, {}, this.animate);
  }

  public details(data:any) {
    let tmp = { data:data };
    this.navCtrl.push(DealDetailsPage, tmp);
  }

  public message(fab) {
    fab.close();
    this.navCtrl.push(MessageListPage);
  }

  public profile(fab) {
    fab.close();
    this.navCtrl.push(ProfilePage);
  }

  public share(fab):void {
    fab.close();
    let options:any = {
      message:
      "1. Click the link below to download the Smartly Biz app today from the Google Play Store: https://play.google.com/store/apps/details?id=com.masology.smartlybiz&ah=YDt8burHAvDGgR1PY0K8LO7keOc "+
      "2. Register yourself by filling the registration form. "+
      "3. Log in to the Smartly Biz app using your email address and Password to explore our amazing deals "+
      "4. Click on this link http://smartlybiz.com/all/"+this.userProvider.getUser()._id+" to subscribe to our store. "+
      "5. Click on follow button to avail discounted deals with “"+this.userProvider.getUser().companyName+"” then refresh your screen. ",
      subject:this.userProvider.getUser().companyName,
      file:this.changeURL(this.userProvider.getUser().qrcodePath),
      url:'',
      chooserTitle:'Share my store...'
    };
    console.log(options);
    this.socialSharing.shareWithOptions(options);
  }

  public delete(id) {
    this.alertCtrl.create({
      title:'Confirmation',
      subTitle:'Are you sure you want to delete this deal?',
      message:'Press okay to confirm this action.',
      buttons:[
        {
          text:'Cancel',
        },
        {
          text:'Okay',
          handler: ()=> {
            this.dealsProvider.deleteDeal({dealId:id}).subscribe(
              response => {
                console.log(response.success);
                if(response.success)
                  this.confirmDelete();
                else
                  this.alertCtrl.create({
                    title:'Error',
                    subTitle:response.msg,
                    message:response.data,
                    buttons:['Okay']
                  }).present();
              },
              error => {
                console.log(error);
                this.alertCtrl.create({
                  title:'Error',
                  subTitle:'Something went wrong.',
                  message:'Check your internet connection and tryagain.',
                  buttons:['Okay']
                }).present();
              }
            );
          }
        }
      ]
    }).present();
  }

  private confirmDelete() {
    this.ngOnInit();
    this.alertCtrl.create({
      title:'Success',
      subTitle:'Deal deleted successfully.',
      enableBackdropDismiss:false,
      buttons:['Okay']
    }).present();
  }

  public edit(data) {
    delete data.createdDate;
    delete data.__v;
    this.navCtrl.setRoot(UploadDealsPage, {data:data}, this.animate);
  }
}
