import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, MenuController } from 'ionic-angular';

import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';

import { UserProvider } from '../../providers/user/user';
import { UserLoginFields, UserLoginFieldsErrors } from '../../interfaces/page.login';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public user: UserLoginFields = {email:'', password:'', gcm_id:'', platform:'' };
  public error: UserLoginFieldsErrors = {e_empty:false, e_valid:false, p_empty:false, p_length:false, p_regex:false};
  private is_loaded:boolean = false;
  public isShow:boolean = false;
  public isPassword:string = 'password';
  public isRemember:boolean = false;
  private device_info:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public userProvider: UserProvider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private menuCtrl: MenuController
              ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.menuCtrl.enable(false, 'myMenu');

    if(localStorage.getItem('shop_credentials') != null) {
      this.isRemember = true;
      this.user = JSON.parse(localStorage.getItem('shop_credentials'));
    }
  }

  public register():void {
    this.navCtrl.push(RegisterPage);
  }

  public showPassword():void {
    this.isShow = !this.isShow;
    if(this.isPassword == 'password')
      this.isPassword = 'text';
    else
    this.isPassword = 'password';
  }

  public signin():void {
    this.device_info = JSON.parse(localStorage.getItem('device_info'));
    this.is_loaded = false;
    if(this.user.email == ''){
      this.error.e_empty = true;
      setTimeout(() => {
        this.error.e_empty = false;
      }, 3000);
    }

    if(this.user.password == ''){
      this.error.p_empty = true;
      setTimeout(() => {
        this.error.p_empty = false;
      }, 3000);
    }

    if(this.error.e_empty == false){
      var x = this.user.email;
      var atpos = x.indexOf("@");
      var dotpos = x.lastIndexOf(".");
      if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        this.error.e_valid = true;
        setTimeout( ()=> {
          this.error.e_valid = false;
        }, 3000);
      }
    }

    if(this.user.password != ''){
      if(this.user.password.length < 5) {
        this.error.p_length = true;
        setTimeout(() => {
          this.error.p_length = false;
        }, 3000);
        return;
      }

      if(/[^A-z\s\d][\\\^]?/.test(this.user.password)) {
        this.error.p_regex = true;
        setTimeout(() => {
          this.error.p_regex = false;
        }, 3000);
        return;
      }
    }
    setTimeout(()=> {
      if(this.device_info != null) {
        this.user.gcm_id = this.device_info.gcm_id;
        this.user.platform = this.device_info.platform;
      }
      else {
        this.user.gcm_id = 'Az98jhsdkf098sdkfjd9f90dsfdsjfklsdj0z'
        this.user.platform = 'Android';
      }

      if(this.isRemember)
        localStorage.setItem('shop_credentials', JSON.stringify(this.user));
      else {
        if(localStorage.getItem('shop_credentials') != null)
          localStorage.removeItem('shop_credentials');
      }

      if(this.error.e_empty == false && this.error.e_valid == false && this.error.p_empty == false && this.error.p_length == false && this.error.p_regex == false) {
        let loader = this.loadingCtrl.create({
          spinner:'dots',
          content:'Please Wait...',
          duration:10000
        });
        loader.present();
        this.userProvider.signin(this.user).subscribe(
          response=> {
            this.is_loaded = true;
            loader.dismiss();
            console.log(response)
            if(response.success) {
              this.userProvider.saveUser(response.data);
              this.navCtrl.setRoot(HomePage);
            }
            else {
              this.alertCtrl.create({
                title:'Error!!',
                subTitle:response.msg,
                message:response.data,
                enableBackdropDismiss:false,
                buttons:['Okay']
              }).present();
            }
          },
          error=> {
            console.error(error);
            loader.dismiss();
          }
        );
        loader.onDidDismiss(()=> {
          if(this.is_loaded)
            return;
          this.alertCtrl.create({
            title:'Error!!',
            subTitle:'Something went wrong. Check your internet connection and tryagain.',
            enableBackdropDismiss:false,
            buttons:['Okay']
          }).present();
        })
      }
    }, 200)
  }
}
