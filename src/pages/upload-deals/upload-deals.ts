import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ActionSheetController, ToastController, LoadingController, AlertController } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';

// import { Keyboard } from '@ionic-native/keyboard';

import { HomePage } from '../home/home';

import { IDealsObj } from '../../interfaces/page.deal';
import { UserProvider } from '../../providers/user/user';
import { DealsProvider } from '../../providers/deals/deals';

@IonicPage()
@Component({
  selector: 'page-upload-deals',
  templateUrl: 'upload-deals.html',
})
export class UploadDealsPage {
  @ViewChild (Slides) slides:Slides;
  public currentIndex:number = 0;
  public data:IDealsObj = {name:'', description:'', startDate:null, endDate:null, storeId:this.userProvider.getUser()._id, image:''};
  public start:any = {date:'', time:''};
  public end:any = {date:'', time:''};
  is_loaded:boolean = false;
  is_edit:boolean = false;
  is_hide:boolean = false;
  animate:any = {animate: true, direction: 'backward'};
  minStartDate:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private camera: Camera,
              private actionSheetCtrl: ActionSheetController,
              private toastCtrl: ToastController,
              private userProvider: UserProvider,
              private dealsProvider: DealsProvider,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private file: File,
              // private keyboard: Keyboard
              ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadDealsPage');
    if(this.navParams.get('data')) {
      this.data = this.navParams.get('data');
      this.start.date = this.convertDateTime(this.data.startDate).toISOString();
      this.start.time = this.convertDateTime(this.data.startDate).toISOString();
      // var date =new Date(Date.now())
      // this.start.date = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
      this.end.date = this.convertDateTime(this.data.endDate).toISOString();
      this.end.time = this.convertDateTime(this.data.endDate).toISOString();

      // console.log(this.start, this.end);
      this.is_edit = true;
    }

    else {
      let date = new Date();
      // var date =new Date(Date.now())
      this.start.date = this.convertDateTime(date).toISOString();
      this.start.time = this.convertDateTime(date).toISOString();
      this.minStartDate = this.convertDateTime(date).toISOString();
      // this.start.date = this.formatDate(this.start.date);
      // this.start.time = this.formattime(this.start.time);
      // this.start.date = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
      console.log(this.minStartDate);
    }
  }

  formatDate(date) {
    let d = new Date(date),
      day = '' + d.getDate(),
      month = '' + (d.getMonth() + 1),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  formattime(date) {

    let d = new Date(date),

      hour = '' + d.getHours(),
      minute = '' + d.getMinutes();
      // year = d.getFullYear();
    // if (month.length < 2) month = '0' + month;
    // if (day.length < 2) day = '0' + day;
    return [hour, minute].join(':');
  }

  public goBack():void {
    this.alertCtrl.create({
      title:'Exit Page!!!',
      subTitle:'Are you sure you want to exit page?',
      buttons:[
        {
          text:'No'
        },
        {
          text:'Yes',
          handler:()=> {
            this.navCtrl.setRoot(HomePage, {}, this.animate);
          }
        }
      ]
    }).present();
  }

  public moveSlide(action): void {
    if(action == 'next')
      this.slides.slideNext();
    else
      this.slides.slidePrev();
  }

  public slideChanged():void {
    // console.log(this.slides.getActiveIndex());
    this.currentIndex = this.slides.getActiveIndex();
  }

  getPicture() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Option',
      buttons: [
        {
          text: 'From Camera',
          handler: () => {
            const options: CameraOptions = {
              quality: 100,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              sourceType: 1,
              allowEdit: true,
              correctOrientation: false,
              saveToPhotoAlbum: false,
              cameraDirection: 1,
              // targetWidth:350,
              // targetHeight:250
            }
            this.camera.getPicture(options).then((imageData) => {
             // imageData is either a base64 encoded string or a file URI
             // If it's base64:
              this.data.image = 'data:image/jpeg;base64,' + imageData;
              console.log('data:image/jpeg;base64,' + imageData)
            }, (err) => {
             // Handle error
             console.error('Error getting image.', err)
            });
            return;
          }
        },
        {
          text: 'From Gallery',
          handler: () => {
            const options: CameraOptions = {
              quality: 100,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              sourceType: 0,
              allowEdit: true,
              correctOrientation: false,
              saveToPhotoAlbum: false,
              cameraDirection: 1,
              // targetWidth:350,
              // targetHeight:250
            }
            this.camera.getPicture(options).then((imageData) => {
             // imageData is either a base64 encoded string or a file URI
             // If it's base64:
              this.data.image = 'data:image/jpeg;base64,' + imageData;
            }, (err) => {
             // Handle error
             console.error('Error getting image.', err)
            });
          }
        },
        {
          text: 'Cancel',
          role: 'destructive'
        }
      ]
    });
    actionSheet.present();
  }

  private convertDateTime(value:any):Date {
    let date = new Date(value);
    return date;
  }

  public post(action):void {
    // this.end.date = this.convertDateTime(this.data.endDate).toISOString();
    //   this.end.time = this.convertDateTime(this.data.endDate).toISOString();

    console.log(this.end)
    console.log(this.start)
    if(this.data.image == '') {
      this.toastCtrl.create({
        message:'Deal cannot be posted. Image required.',
        position: 'middle',
        duration:5000
      }).present().then(()=> {
        this.slides.slideTo(0, 500);
      });
      return;
    }
    if(this.data.name == '' || this.data.description == '') {
      this.toastCtrl.create({
        message:'Deal cannot be posted. Fields are required.',
        position: 'middle',
        duration:5000
      }).present().then(()=> {
        this.slides.slideTo(1, 500);
      });
      return;
    }
    if(this.start.date == '' || this.start.date == 'NaN-NaN-NaN' || this.start.time == '' ||this.start.time == 'NaN:NaN' || this.end.date == '' || this.end.time == '') {
      this.toastCtrl.create({
        message:'Deal cannot be posted. Fields are required.',
        position: 'middle',
        duration:5000
      }).present();
    }
    else {
      this.start.date =  this.formatDate(this.start.date);
      this.start.time = this.formattime(this.start.time);
      console.log(this.data)
      if(action == 'new') {
        this.data.startDate = this.convertDateTime(this.start.date+' '+this.start.time);
        this.data.endDate = this.convertDateTime(this.end.date+' '+this.end.time);
      }
      else {
        this.data.startDate = this.convertDateTime(this.start.date);
        this.data.endDate = this.convertDateTime(this.end.date);
      }
      let loader = this.loadingCtrl.create({
        content:'Posting Deal...',
        spinner:'dots',
        duration:30000
      });
      loader.present();

      setTimeout(()=> {
        this.is_hide = true;
        if(action == 'new') {
          this.dealsProvider.postNewDeal(this.data).subscribe(
            response=> {
              this.is_loaded = true;
              loader.dismiss();
              this.is_hide = false;
              if(response.success) {
                if(response.status == 401) {
                  this.alertCtrl.create({
                    title:'Error!!',
                    subTitle:response.msg,
                    message:response.data,
                    buttons:['Okay']
                  }).present();
                }
                else {
                  this.adjustCreditPoints();
                  this.navCtrl.setRoot(HomePage, {}, this.animate);
                  this.toastCtrl.create({
                    message:'Deal posted successfully.',
                    duration:5000,
                    position:'middle'
                  }).present();
                }
              }
              else {
                this.alertCtrl.create({
                  title:response.msg,
                  subTitle:response.data,
                  enableBackdropDismiss:false,
                  buttons:['Ok']
                }).present();
              }

            },
            error=> {
              console.error(error);
              loader.dismiss();
            }
          );
        }
        else {
          this.dealsProvider.editDeal(this.data).subscribe(
            response=> {
              this.is_loaded = true;
              loader.dismiss();

              if(response.success) {
                if(response.status == 401) {
                  this.alertCtrl.create({
                    title:'Error!!',
                    subTitle:response.msg,
                    message:response.data,
                    buttons:['Okay']
                  }).present();
                }
                else {
                  this.navCtrl.setRoot(HomePage, {}, this.animate);
                  this.toastCtrl.create({
                    message:'Deal edited successfully.',
                    duration:5000,
                    position:'middle'
                  }).present();
                }
              }
              else {
                this.alertCtrl.create({
                  title:response.msg,
                  subTitle:response.data,
                  enableBackdropDismiss:false,
                  buttons:['Ok']
                }).present();
              }
            },
            error=> {
              console.error(error);
              loader.dismiss();
            }
          );
        }

        loader.onDidDismiss(()=> {
          if(this.is_loaded)
            return;
          this.alertCtrl.create({
            title:'Something went wrong.',
            subTitle:'Check your internet connection and tryagain later.',
            enableBackdropDismiss:false,
            buttons:['Ok']
          }).present();
        })
      },100)
    }
  }

  private adjustCreditPoints() {
    let adjustedPoints = this.userProvider.getUser();
    adjustedPoints.credits = adjustedPoints.credits - 2;
    this.userProvider.saveUser(adjustedPoints);
  }
}
