import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadDealsPage } from './upload-deals';

@NgModule({
  declarations: [
    UploadDealsPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadDealsPage),
  ],
})
export class UploadDealsPageModule {}
