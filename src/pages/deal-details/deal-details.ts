import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { DomSanitizer } from '@angular/platform-browser';

import { GlobalVariablesProvider } from '../../providers/global-variables/global-variables';

@IonicPage()
@Component({
  selector: 'page-deal-details',
  templateUrl: 'deal-details.html',
})
export class DealDetailsPage {

  public data:any = this.navParams.get('data');
  private month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  private day = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _DomSanitizationService: DomSanitizer,
              public globalVariables: GlobalVariablesProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DealDetailsPage');
    console.log(this.data);
  }

  convertDate(date:any) {
    let d = new Date(date);
    let correctDate = this.day[d.getDay()]+' '+d.getDate()+' - '+this.month[d.getMonth()]+' - '+ d.getFullYear();
    return correctDate;
  }
}
