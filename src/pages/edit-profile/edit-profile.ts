import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController, AlertController } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { UserProvider } from '../../providers/user/user';


@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  public data:any = this.navParams.get('data');
  private is_loaded:any = false;
  public isPassword:string = 'password';
  public isShow:boolean = false;
  public token:string = this.userProvider.getUser().token;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private camera: Camera,
              public actionSheetCtrl: ActionSheetController,
              private userProvider: UserProvider,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

  public showPassword():void {
    this.isShow = !this.isShow;
    if(this.isPassword == 'password')
      this.isPassword = 'text';
    else
    this.isPassword = 'password';
  }

  getPicture() {
  	let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Option',
      buttons: [
        {
          text: 'From Camera',
          handler: () => {
            const options: CameraOptions = {
              quality: 100,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              sourceType: 1,
              allowEdit: true,
              correctOrientation: false,
              saveToPhotoAlbum: false,
              cameraDirection: 1,
              targetWidth:200,
              targetHeight:200
            }
            this.camera.getPicture(options).then((imageData) => {
             // imageData is either a base64 encoded string or a file URI
             // If it's base64:
             // console.log(this.pimage);
            }, (err) => {
             console.log(err)
            });
          }
        },
        {
          text: 'From Gallery',
          handler: () => {
            const options: CameraOptions = {
              quality: 100,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              sourceType: 0,
              allowEdit: true,
              correctOrientation: false,
              saveToPhotoAlbum: false,
              cameraDirection: 1,
              targetWidth:200,
              targetHeight:200
            }
            this.camera.getPicture(options).then((imageData) => {
             // imageData is either a base64 encoded string or a file URI
             // If it's base64:
             // console.log(this.pimage);
            }, (err) => {
             console.log(err)
            });
          }
        },
        {
          text: 'Cancel',
          role: 'destructive'
        }
      ]
    });
    actionSheet.present();
  }

  public save(data):void {
    delete data.liked_by;
    delete data.is_premium;
    delete data.gcm_id;
    delete data.platform;
    delete data.createdDate;
    delete data.__v;
    delete data.qrcodePath;

    let laoder = this.loadingCtrl.create({
      spinner:'dots',
      content:'Please Wait...',
      duration:10000,
      dismissOnPageChange:true,
      enableBackdropDismiss:false
    });
    laoder.present();

    this.userProvider.edit(data).subscribe(
      response=> {
        this.is_loaded = true;
        laoder.dismiss();
        console.log(response)
        if(response.success == true) {
          response.data.token = this.token;
          this.userProvider.saveUser(response.data);
          this.navCtrl.pop();
        }
      },
      error=> {
        console.error(error);
        laoder.dismiss();
      }
    );

    laoder.onDidDismiss(()=> {
      if(this.is_loaded)
        return;
      this.alertCtrl.create({
        title:'Error!!',
        subTitle:'Something went wrong.',
        message:'Tryagain later.',
        buttons:['okay'],
        enableBackdropDismiss:false
      }).present();
    });
  }
}
