import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';

import { EditProfilePage } from '../edit-profile/edit-profile';

import{ UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public token:string = this.userProvider.getUser().token;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public userProvider: UserProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.userProvider.getUpdatedUser().subscribe(
      response=>{
        if(response.success == true) {
          response.data.token = this.token;
          this.userProvider.saveUser(response.data);
        }
      },
      error=> {
        console.error(error);
      }
    );
  }

  edit() {
    this.navCtrl.push(EditProfilePage, {data:this.userProvider.getUser()});
  }
}
