import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  private is_loaded:boolean = false;
  private device_info:any = JSON.parse(localStorage.getItem('device_info'));
  public user:any = { fullName:'', userName:'', email:'', mobile:'', password:'', companyName:'', websiteUrl:'', facebookUrl:'', is_premium:true, gcm_id:'', platform:'', credits:50 };
  public error:any = { e_empty:false, e_valid:false, p_empty:false, p_length:false, p_regex:false, u_empty:false, f_empty:false, f_valid:false, m_empty:false, m_length:false, c_empty:false, w_regex:false, f_regex:false };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userProvider: UserProvider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    console.log('New hello new');
    console.log(this.device_info);

    if(this.device_info) {
      this.user.gcm_id = this.device_info.gcm_id;
      this.user.platform = this.device_info.platform;
    }
    else {
      this.user.gcm_id = 'Az98jhsdkf098sdkfjd9f90dsfdsjfklsdj0z'
      this.user.platform = 'Android';
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  public register():void {
    if(this.user.fullName == '') {
      this.error.f_empty = true;
      setTimeout(() => {
        this.error.f_empty = false;
      }, 5000);
    }

    if(this.user.fullName != '') {
      if(/[^A-z\s\d][\\\^]?/.test(this.user.fullName)) {
        this.error.f_valid = true;
        setTimeout(() => {
          this.error.f_valid = false;
        }, 5000);
        return;
      }
    }

    if(this.user.userName == '') {
      this.error.u_empty = true;
      setTimeout(() => {
        this.error.u_empty = false;
      }, 5000);
    }

    if(this.user.email == '') {
      this.error.e_empty = true;
      setTimeout(() => {
        this.error.e_empty = false;
      }, 5000);
    }

    if(this.user.email != ''){
      var x = this.user.email;
      var atpos = x.indexOf("@");
      var dotpos = x.lastIndexOf(".");
      if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        this.error.e_valid = true;
        setTimeout( ()=> {
          this.error.e_valid = false;
        }, 5000);
      }
    }

    if(this.user.password == ''){
      this.error.p_empty = true;
      setTimeout(() => {
        this.error.p_empty = false;
      }, 5000);
    }

    if(this.user.password != ''){
      if(this.user.password.length < 5) {
        this.error.p_length = true;
        setTimeout(() => {
          this.error.p_length = false;
        }, 5000);
        return;
      }

      // if(/[^A-z\s\d][\\\^]?/.test(this.user.password)) {
      //   this.error.p_regex = true;
      //   setTimeout(() => {
      //     this.error.p_regex = false;
      //   }, 5000);
      //   return;
      // }
    }

    // if(this.user.mobile == ''){
    //   this.error.m_empty = true;
    //   setTimeout(() => {
    //     this.error.m_empty = false;
    //   }, 5000);
    // }

    // if(this.user.companyName == ''){
    //   this.error.c_empty = true;
    //   setTimeout(() => {
    //     this.error.c_empty = false;
    //   }, 5000);
    // }

    // if(this.user.mobile != ''){
    //   if(this.user.mobile.length < 11) {
    //     this.error.m_length = true;
    //     setTimeout(() => {
    //       this.error.m_length = false;
    //     }, 5000);
    //     return;
    //   }
    // }

    // if(this.user.websiteUrl != ''){
    //   if(!/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(this.user.websiteUrl)) {
    //     this.error.w_regex = true;
    //     setTimeout(() => {
    //       this.error.w_regex = false;
    //     }, 5000);
    //   }
    // }

    // if(this.user.facebookUrl != ''){
    //   if(!/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(this.user.facebookUrl)) {
    //     this.error.f_regex = true;
    //     setTimeout(() => {
    //       this.error.f_regex = false;
    //     }, 5000);
    //     return;
    //   }
    // }
    setTimeout(() => {
      if(this.error.e_empty == false && this.error.e_valid == false && this.error.p_empty == false && this.error.p_length == false && this.error.p_regex == false && this.error.u_empty == false && this.error.f_empty == false && this.error.f_valid == false && this.error.m_empty == false && this.error.m_length == false) {
        const loader = this.loadingCtrl.create({
          spinner:'dots',
          content:'Please Wait...',
          duration:10000
        });
        loader.present();

        this.userProvider.signup(this.user).subscribe(
          response=> {
            this.is_loaded = true;
            loader.dismiss();
            if(response.success)
              this.alertCtrl.create({
                title:'Success!!',
                subTitle: response.msg,
                enableBackdropDismiss:false,
                buttons:[
                  {
                    text:'Ok',
                    handler: ()=> {
                      this.navCtrl.pop();
                    }
                  }
                ]
              }).present();
            else
              this.alertCtrl.create({
                title:response.msg,
                subTitle: response.data,
                enableBackdropDismiss:false,
                buttons:['Ok']
              }).present();
          },
          error => {
            loader.dismiss();
            console.error(error);
          }
        );
        loader.onDidDismiss(()=> {
          if(this.is_loaded)
            return;
          this.alertCtrl.create({
            title:'Error!!',
            subTitle:'Something went wrong. Tryagain later.',
            enableBackdropDismiss:false,
            buttons:['Ok']
          }).present();
        })
      }
    }, 200);
  }
}
