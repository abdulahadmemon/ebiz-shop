import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { SendMessagePage } from '../send-message/send-message';

import { MessageProvider } from '../../providers/message/message';
import { UserProvider } from '../../providers/user/user';
import { GlobalVariablesProvider } from '../../providers/global-variables/global-variables';

@IonicPage()
@Component({
  selector: 'page-message-list',
  templateUrl: 'message-list.html',
})
export class MessageListPage {

  private userId = this.userProvider.getUser()._id;
  private i: number = 0;
  public data: any = [];
  public is_loaded: boolean = false;
  type: any = this.navParams.get('type');

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private messageProvider: MessageProvider,
    private userProvider: UserProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private _DomSanitizationService: DomSanitizer,
    public globalVariables: GlobalVariablesProvider
  ) {
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad MessageListPage');
    console.log(this.navParams.data)

    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Loading messages...',
      enableBackdropDismiss: false,
      duration: 10000
    });
    loader.present();

    await this.messageProvider.getList({ storeId: this.userId }).subscribe(
      response => {
        this.is_loaded = true;
        loader.dismiss();
        console.log(response);
        this.getImages(response.data);
        // this.data = response.data;
      },
      error => {
        console.error(error);
        loader.dismiss();
      }
    );

    loader.onDidDismiss(() => {
      if (this.is_loaded)
        return;
      this.alertCtrl.create({
        title: 'Error!!',
        subTitle: 'Something went wrong.',
        message: 'Please tryagain.',
        buttons: ['Okay']
      }).present();
    });
    console.log(this.data)

    // console.log(this.data);
  }

  public getImages(data): void {

    let length = data.length;
    if (length >= (this.i + 1)) {
      this.messageProvider.getImage({ dealId: data[this.i].dealId, userId: data[this.i].userId }).subscribe(
        response => {
          data[this.i].image = this.changeURL(response.data.image);
          data[this.i].name = response.data.dealName;
          data[this.i].userName = response.data.userName;

          console.log(data[this.i])
          var count = 0;
          for (var i = 0; i < data[this.i].message.length; i++) {
            console.log(data[this.i].message[i])
            if (data[this.i].message[i].status === 1 && data[this.i].message[i].type === 'user') {
              count = count + 1;
            }
            else {
              count = count;
            }
          }
          data[this.i].count = count
          console.log(count)
          this.data.push(data[this.i]);

          this.i++;
          this.getImages(data);
        },
        error => {
          console.error(error);
        }
      )

      // console.log(this.data)
    }
    else {
      console.log('else');
      this.is_loaded = true;
      // this.loader.dismiss();
      console.log(data);
      // this.data = data;
      if (this.type == 'pushData') {
        let tmpData = this.navParams.get('notiData');
        console.log(this.data[0].dealId)
        let image = this.data.find(x => x.dealId === tmpData.dealId);
        console.log(image)
        this.sendMessage(tmpData.dealId, tmpData.storeId, image.image, tmpData.userId);
        // console.log(data);
        // this.data = data;
        // if(this.type == 'pushData') {
        //   let tmpData = this.navParams.get('notiData');
        //   let image = this.data.find(x => x._id === tmpData.dealId);
        //   this.sendMessage(tmpData.dealId, tmpData.storeId, image.image ,tmpData.userId);
      }

    }
  }

  public sendMessage(dealId, storeId, image, userId): void {
    let tmp = { dealId: dealId, storeId: storeId, userId: userId };
    this.navCtrl.push(SendMessagePage, { data: tmp, image: image });
  }

  private changeURL(image): SafeUrl {
    if (image.includes('skapi') || image.includes('192')) {
      let tmp = image;
      return (tmp);
    }
    else {
      let tmp = this.globalVariables.getEndpoint() + image;
      return (tmp);
    }
    // let sanitize = this._DomSanitizationService.bypassSecurityTrustUrl(tmp);
    // console.log(tmp);
  }
}
