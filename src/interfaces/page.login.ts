export interface UserLoginFields {
    email:string,
    password:string,
    gcm_id:string,
    platform:string
}

export interface UserLoginFieldsErrors {
    e_empty:boolean,
    e_valid:boolean,
    p_empty:boolean,
    p_length:boolean,
    p_regex:boolean
}
