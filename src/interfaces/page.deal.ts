export interface IDealsObj {
    name: string,
    description: string,
    startDate: any,
    endDate: any,
    image: string,
    storeId: string,
}
